export class Contact {

    name : string;
    id: number;
    companyName: string;
    isFavorite: boolean;
    smallImageURL : string;
    largeImageURL: string;
    emailAddress: string;
    birthdate: Date;
    phone: Phone;
    address: Address;

    constructor(){

    }

}
class Phone{
    work: string;
    home: string;
    mobile: string;

    constructor(work: string, home: string, mobile: string){
        this.work = work;
        this.home = home;
        this.mobile = mobile;
    }
}
class Address{
    street: string;
    city: string;
    state: string;
    country: string; 
    zipCode: string;

    constructor(street: string, city: string, state: string, country: string, zipCode: string){
        this.street = street;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zipCode = zipCode;
    }
}
