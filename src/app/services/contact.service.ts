import { Contact } from '../models/contact/contact';
import {Injectable} from "@angular/core"
import {HttpClient,HttpHeaders  } from "@angular/common/http"
import { HTTP } from '@ionic-native/http/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
    providedIn: "root"
})
export class ContactService{

    private _urlContacts = "https://storage.googleapis.com/quares-front-challenge/contacts.json";
    private _contacts: Contact[] = [];
    
    constructor(private http: HttpClient , private httpNative: HTTP,public platform: Platform){
        if(this.platform.is("android") || this.platform.is("ios")){
            this.httpNative.get(this._urlContacts, {}, {})
            .then(data => {
                this._contacts = JSON.parse(data.data)
                console.log(data.status);
                console.log(data.data); // data received by server
                console.log(data.headers);
            })
            .catch(error => {
                alert("error");
                console.log(error.status);
                console.log(error.error); // error message as string
                console.log(error.headers);
          
            });
        } else{
            this.http.get(this._urlContacts, { headers: { 'content-type': 'application/json' } }).subscribe(
                (data: Contact[]) => {
                    console.log(data);
                    this._contacts = data;
                },
                error => {
                    alert( error.message);
                },
                  () => {
                   alert("nada")
            })
        }    
    }
    
    getContact(id: any): Contact {
        return this._contacts.find(contact => contact.id === id);
    }

    getAllContacts(): Contact[]{
        return this._contacts;
    }

    getFavoriteContacts(): Contact[]{
        return this._contacts.filter (x => x.isFavorite == true);
    }

    getOtherContacts(): Contact[]{
        return this._contacts.filter (x => x.isFavorite == false);
    }
    
}