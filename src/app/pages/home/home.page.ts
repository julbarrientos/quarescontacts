import { Component, ChangeDetectorRef } from '@angular/core';
import {ContactService} from '../../services/contact.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public contactService: ContactService) {}

  ngOnInit(){
   console.log("ngOnInit");
  }
}
