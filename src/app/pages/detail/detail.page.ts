import { ContactService } from '../../services/contact.service';
import { Contact } from './../../models/contact/contact';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-detail',
  templateUrl: 'detail.page.html',
  styleUrls: ['detail.page.scss'],
})
export class DetailPage {

  public contact : Contact;
  public favoriteIconTrue = "../../../assets/Favorite_Star_True/Favorite_True.png";
  public favoriteIconFalse = "../../../assets/Favorite_Star_False/Favorite_False.png";

  private _loading: any;
  constructor(private _route: ActivatedRoute, private _contactService: ContactService, public loadingController: LoadingController) {}

  ngOnInit() {
    this.presentLoading();
  }

  async presentLoading() {
    this._loading = await this.loadingController.create({
      message: 'Cargando'
    });
    return this._loading.present();

  }
  imgLoaded(){
    this._loading.dismiss();
  }
  ionViewWillEnter(){
    let contactId = this._route.snapshot.paramMap.get('id');
    this.contact = this._contactService.getContact(contactId);
  }
  makeFavorite(){
    this.contact.isFavorite = !this.contact.isFavorite;
  }
  imgError(event: any){
    event.currentTarget.src='../../assets/UserLarge/User_Large.png';
  }
}
